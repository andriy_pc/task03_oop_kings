package com.supermarket;

import java.util.Scanner;

/* 1) Supermarket.java - публічний клас - метод main в якомусь лівому класі.
 * 2) Він казав все має бути реалізовано по схемі MVC - я не впевнений що в тебе саме так;
 * адже необхідно щоб був окремий клас Controller, через який користувач буде взаємодіяти
 * з програмою. Також там має бути клас View, який відображатиме текст на екрані саме для
 * інтерактиву; має бути класс Model - в якій повинна бути реалізована бізнес-логіка
 * і, я так думаю, робота з данними;
 * 3) В тебе все extends від Supermarket. Я думаю краще було б зробити окремий абстрактний класс
 * для моделювання відділу, від нього extends три тих відділи, а в Supermarket, композицією чи
 * агрегацією, втулити ці відділи, ну і дати супермаркету методи для того щоб працювати з ними.
 * Тоді Supermarket буде частина моделі, яка відповідає за логіку. Ті підкласи відділи - за
 * дані, ну і зробити якійсь додатковий клас взаємодії з користувачем, і мб окремий клас,
 * на який ти передаватимеш текст, щоб той його виводив, щоб очевидно було що дз виконано
 * по схемі MVC.
 * 4) В класі Supermarket в goToRemoval в if ти щоразу створюєш новий об'єкт, думаю краще зробити
 * масив цих об'єктів і звертатися до них по індексу, так ти не забиватимеш пам'ять;
 * 5) Ти в похідних від Supermarket зробив масив з товарами, і не використовуєш його...
 * 6) choise - choice;
 * 7) Він радив то робити в різних пакетах, або хоча б в різних класах;
 * 8) Він казав щоразу необхідно через maven робити сайт, щоб плагіни показували на неправильний стиль,
 * і ми маємо виправляти ці помилки.*/
class Player{
    public static void main(String[] args) {
        Supermarket sp = new Supermarket();
        int halt;
        do{
            halt = sp.goToRemoval();
        } while(halt != 4);
    }
}

public class Supermarket {
    Scanner sc = new Scanner(System.in);
    public int goToRemoval(){
        System.out.println("1. Products Removal");
        System.out.println("2. SweetShop");
        System.out.println("3. Clothes");
        System.out.println("4. Quit");
        int choise = sc.nextInt();
        if(choise == 1) {
            Supermarket playerLocation = new FoodRemoval();
            playerLocation.getProducts();
        }else if(choise == 2) {
            Supermarket playerLocation = new SweetShop();
            playerLocation.getProducts();
        }else if(choise == 3) {
            Supermarket playerLocation = new Clothes();
            playerLocation.getProducts();
        }
        return choise;
    }
    public void getProducts(){

    }
}

class FoodRemoval extends Supermarket{
    String products[] = {"Fish", "Meat", "Milk"};

    @Override
    public void getProducts() {
        System.out.println("1. Fish");
        System.out.println("2. Meat");
        System.out.println("3. Milk");
        System.out.println("4. Quit");
        int choise = sc.nextInt();
        if(choise == 1){
            System.out.println("You bought fish");
        }else if(choise == 2){
            System.out.println("You bought meat");
        }else if(choise == 3){
            System.out.println("You bought milk");
        }
        return;
    }
}

class SweetShop extends Supermarket{
    String products[] = {"Candies", "Cake", "Chocolate"};

    @Override
    public void getProducts() {
        System.out.println("1. Candies");
        System.out.println("2. Cake");
        System.out.println("3. Milk");
        System.out.println("4. Chocolate");
        int choise = sc.nextInt();
        if(choise == 1){
            System.out.println("You bought Candies");
        }else if(choise == 2){
            System.out.println("You bought Cake");
        }else if(choise == 3){
            System.out.println("You bought Chocolate");
        }
        return;
    }
}

class Clothes extends Supermarket{
    String products[] = {"Pants", "T-Shirts", "Fleep-Flops"};

    @Override
    public void getProducts() {
        System.out.println("1. Pants");
        System.out.println("2. T-Shirts");
        System.out.println("3. Fleep-Flops");
        System.out.println("4. Quit");
        int choise = sc.nextInt();
        if(choise == 1){
            System.out.println("You bought Pants");
        }else if(choise == 2){
            System.out.println("You bought T-Shirts");
        }else if(choise == 3){
            System.out.println("You bought Fleep-Flops");
        }
        return;
    }
}